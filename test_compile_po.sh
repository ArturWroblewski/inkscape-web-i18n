#!/bin/bash

# django cannot deal with files with a byte order mark
has_bom() { head -c3 "$1" | grep -q $'\xef\xbb\xbf'; }

# get all po files, and count them
files_counted=$(find . -name "*.po" | wc -l)
echo "$files_counted files found to test."

START_TIME=$(date +%s%N)
count=0
# get all po files, and check for bom
find . -name "*.po" | while read line
do
        
        (( count++ ))
        if has_bom $line; then
                echo "Byte order mark found in $line!"
                exit 1
        fi
        output_dir=$(dirname $line)
        msgfmt --check-format -o "$output_dir/django.mo" $line
        if [ $? -ne 0 ]; then
                echo "($count/$files_counted) $line failed to compile!"
                exit 1
        else
                echo "($count/$files_counted) $line ......   Passed"
        fi 
done

echo "Testing complete in $(echo "scale=10;($(date +%s%N) - ${START_TIME})/(1*10^09)" | bc) sec."
